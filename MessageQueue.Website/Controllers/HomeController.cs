﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using MessageQueue.Website.Models;
using Newtonsoft.Json;
using msmq = System.Messaging;

namespace MessageQueue.Website.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(UnsubcribeModel model)
        {
            var cmd = new UnsubcribeModel { Email = model.Email };

            using (var queue = new msmq.MessageQueue(".\\private$\\davidequeue"))
            {
                var message = new msmq.Message();
                var jsonBody = JsonConvert.SerializeObject(cmd);

                message.BodyStream = new MemoryStream(Encoding.Default.GetBytes(jsonBody));

                queue.Send(message);
            }

            return View("Confirmation");
        }
        [HttpPost]
        public ActionResult UnsubscribeWithTransaction(UnsubcribeModel model)
        {
            var cmd = new UnsubcribeModel { Email = model.Email };

            using (var queue = new msmq.MessageQueue(".\\private$\\davidequeue-transaction"))
            {
                var message = new msmq.Message();
                var jsonBody = JsonConvert.SerializeObject(cmd);

                message.BodyStream = new MemoryStream(Encoding.Default.GetBytes(jsonBody));

                // Create transaction
                using (var transaction = new msmq.MessageQueueTransaction())
                {
                    try
                    {
                    //Start transaction
                    transaction.Begin();

                    // send message with the transaction.
                    queue.Send(message, transaction);

                    //commit transaction.
                    transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Abort();
                    }
                }
            }

            return View("Confirmation");
        }
    }
}