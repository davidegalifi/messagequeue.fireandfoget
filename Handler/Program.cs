﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;
using Newtonsoft.Json;
using Service;
using msmq = System.Messaging;

namespace Handler
{
    public class Program
    {
        public static void Main(string[] args)
        {
            using (var queue = new msmq.MessageQueue(".\\private$\\davidequeue"))
            {
                while (true)
                {
                    Console.WriteLine("Listening...");

                    var message = queue.Receive();

                    Console.WriteLine("Working...");

                    using (var reader = new StreamReader(message.BodyStream))
                    {
                        var objJson= reader.ReadToEnd();

                        var cmd = JsonConvert.DeserializeObject<UnsubscribeCommand>(objJson);

                        // Service with business Logic
                        var srv = new UnsubscribingService();

                        srv.Unsubscribe(cmd);

                        Console.WriteLine(string.Format("User: {0} unsubscribed", cmd.Email));
                    }
                }
            }
        }
    }
}
